import React from 'react';
import { View, TextInput, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { passwordChecker } from '../Action/TextInputCheckerAction';


class TextInputChecker extends React.Component {
    
    checker(){
        const passwordParam = "^(?=.{5,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"
        if (this.props.password.match(passwordParam)) {
            alert("True")
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.checker.bind(this)}>
                    <Text>{this.props.password}</Text>
                </TouchableOpacity>
                <TextInput
                    onChangeText={(text) => this.props.passwordChecker(text)}
                    value={this.props.password}
                    style={styles.inputStyle} 
                />
            </View>
        )
    }
}

const mapStateToProps = state => {
    const { password } = state.textInputCheckerReducer 
    return { password }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputStyle: {
        width: "98%",
        height: 40,
        borderWidth: 1,
        borderColor: "gray"
    }
})

export default connect(mapStateToProps, { passwordChecker })(TextInputChecker);
