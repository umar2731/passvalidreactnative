import { PASSWORD_CHANGED } from './types';

export const passwordChecker = (text) => {
    return {
        type: PASSWORD_CHANGED,
        payload: text
    }
}
