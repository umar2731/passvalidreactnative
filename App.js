import React from 'react';
import { createStore } from "redux";
import { Provider } from "react-redux";
import reducer from './src/Reducer/index';

import TextInputChecker from './src/Component/TextInputChecker';

class App extends React.Component {
  render() {
    return (
      <Provider store={createStore(reducer)}>
        <TextInputChecker />
      </Provider>
    )
  }
}

export default App;
